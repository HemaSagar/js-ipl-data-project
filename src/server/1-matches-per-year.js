function matchesPerYear(matches) {
    const matchesPerYear = matches.reduce((result, match) => {
        if (result[match.season]) {
            result[match.season] += 1;
        }
        else {
            result[match.season] = 1
        }
        return result;
    }, {});

    return matchesPerYear;
}

module.exports = matchesPerYear;

