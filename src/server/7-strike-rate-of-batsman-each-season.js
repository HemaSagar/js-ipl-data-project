function strikeRateForEachSeason(matches, deliveries) {

    let matchId = matches.reduce((result,match) => {
        if (result[match.season]) {
            result[match.season].push(match.id)
        }
        else {
            result[match.season] = [match.id];
        }
        return result;
    },{})

    // finds runs scored and ballsfaced for each batsmen for every season
    let playerDetails = Object.keys(matchId).reduce((forYear, matchYear) => {

        forYear[matchYear] = deliveries.filter((delivery) => {
            return matchId[matchYear].includes(delivery.match_id);
        })
            .reduce((result, delivery) => {

                if (result[delivery.batsman]) {
                    result[delivery.batsman]['runs'] = (result[delivery.batsman]['runs'] ?? 0) + Number(delivery.batsman_runs);
                    result[delivery.batsman]['balls'] = (result[delivery.batsman]['balls'] ?? 0) + 1;

                }
                else {
                    result[delivery.batsman] = { 'runs': Number(delivery.batsman_runs), 'balls': 1 }
                }
                if (Number(delivery.wide_runs) > 0) {
                    result[delivery.batsman]['balls'] -= 1;
                }
                return result;
            }, {})
        return forYear;
    }, {})

    // calculating strikeRate of each batsmen for every season
    const strikeRateEachSeason = Object.keys(playerDetails).reduce((result, matchYear) => {
        result[matchYear] = Object.keys(playerDetails[matchYear]).reduce((strikeRate, player) => {
            strikeRate[player] = {...playerDetails[matchYear][player]};
            strikeRate[player]['strikeRate'] = ((playerDetails[matchYear][player]['runs'] / playerDetails[matchYear][player]['balls']) * 100).toFixed(2);
            return strikeRate;
        }, {})
        return result;

    }, {})
    return strikeRateEachSeason;

}

module.exports = strikeRateForEachSeason;