function highestPlayerOfMatches(matches) {
    let playerOfTheMatch = {};
    let highestAwards = matches.filter((match)=>{
        return match.player_of_match;
    })
    .reduce((result,match)=>{
        if(playerOfTheMatch[match.season]){
            playerOfTheMatch[match.season][match.player_of_match] = (playerOfTheMatch[match.season][match.player_of_match] ?? 0) + 1
        }
        else{
            playerOfTheMatch[match.season]= {};
            result.push(match.season);
        }
        return result;
    },[])
    .reduce((result,matchYear) =>{
        let players = Object.keys(playerOfTheMatch[matchYear]).sort((player1,player2)=>{

            return playerOfTheMatch[matchYear][player2] - playerOfTheMatch[matchYear][player1];
        })
        
        result[matchYear] = {[players.slice(0,1)]:playerOfTheMatch[matchYear][players.slice(0,1)]}
            
        return result;
    },{});
    return highestAwards;

}


module.exports = highestPlayerOfMatches;