function playerDismissedByPlayersHighest(deliveries) {

    //calculates how many times a bowler dismissed a particular batsmen
    let playerDismissals = deliveries.reduce((result,delivery) => {
        if (delivery.player_dismissed) {
            let batsman = delivery.batsman;
            let bowler = delivery.bowler; 
            if (result[batsman]) {
                if (result[batsman][bowler]) {
                    result[batsman][bowler] += 1;
                }
                else {
                    result[batsman][bowler] = 1
                }
            }
            else {
                result[batsman] = { [bowler]: 1 };
            }
        }
        return result;
    },{})

    let maxDismissed = -1;
    
    // finds out which bowler dismissed which batsmen the most times
    let highestDismissedPlayer = Object.keys(playerDismissals)
        .reduce((result, player) => {
            let maxBowler = Object.keys(playerDismissals[player]).sort((bowler1, bowler2) => {
                return playerDismissals[player][bowler2] - playerDismissals[player][bowler1];
            }).slice(0, 1);

            if(playerDismissals[player][maxBowler]>maxDismissed){
                maxDismissed = playerDismissals[player][maxBowler]
                result = {[player]:{[maxBowler]:playerDismissals[player][maxBowler]}};
            }

            
            return result;
        }, {})

    return highestDismissedPlayer;

}

module.exports = playerDismissedByPlayersHighest;