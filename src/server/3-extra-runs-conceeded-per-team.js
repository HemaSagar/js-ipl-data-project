function extraRunsCalculate(matches, deliveries, year = 2016) {
    const matchYear = year;
    let extraRuns = {};
    const matchIds = matches.filter((match) => {
        return Number(match.season) === matchYear;
    })
    .map((match) => {
        return Number(match.id);
    })

    extraRuns = deliveries.filter((delivery) => {
        return matchIds.includes(Number(delivery.match_id))
    })
    .reduce((result, delivery) => {
        result[delivery.bowling_team] = (result[delivery.bowling_team] ?? 0) + Number(delivery.extra_runs);
        return result;
    }, {})

    return extraRuns;
}


module.exports = extraRunsCalculate;
