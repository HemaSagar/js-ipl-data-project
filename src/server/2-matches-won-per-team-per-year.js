function matchesWonByTeamPerYear(matches) {

    const wonPerTeamPerYear = matches.reduce((result, match) => {
        if (result[match.winner]) {
            result[match.winner][match.season] = (result[match.winner][match.season] ?? 0) + 1;
        }
        else {
            result[match.winner] = {};
        }
        return result;
    }, {})

    return wonPerTeamPerYear;
}

module.exports = matchesWonByTeamPerYear;
