function economicalBowlers(matches, deliveries, year = 2015) {
    const matchYear = year;
    let bowlerDetails = {};
    let topTenEconimicalBowlers = {};

    //matchIds store match ids of given year.
    const matchIds = matches.reduce((result, match) => {
        if (Number(match.season) === matchYear) {
            result.push(match.id);
        }
        return result;
    }, []);

    //filter out matches only from given year and reduce findsout runs and balls for every bowler and return an object
    bowlerDetails = deliveries.filter((delivery) => {
        return matchIds.includes(delivery.match_id)
    })
    .reduce((result, delivery) => {
        if (result[delivery.bowler]) {
            result[delivery.bowler].runs += Number(delivery.total_runs)
            result[delivery.bowler].balls += 1
        }
        else {
            result[delivery.bowler] = { runs: Number(delivery.total_runs), balls: 1 };
        }
        return result;

    }, {})

    //economy is added to bowler Details
    bowlerDetails = Object.keys(bowlerDetails).reduce((result, bowler) => {
        let economy = Number((bowlerDetails[bowler].runs / (bowlerDetails[bowler].balls / 6)).toFixed(2));
        result[bowler] = {...bowlerDetails[bowler],'economy':economy};
        return result;
    },{})

    //bowlerDetails is sorted by economy and top 10 bowlers are stored into result
    topTenEconimicalBowlers = Object.keys(bowlerDetails).sort((a, b) => {
        return bowlerDetails[a].economy - bowlerDetails[b].economy;
    })
    .slice(0,10)
    .reduce((result,bowler) => {
        result[bowler] = bowlerDetails[bowler];
        return result;
    },{})
    return topTenEconimicalBowlers;
}

module.exports = economicalBowlers;