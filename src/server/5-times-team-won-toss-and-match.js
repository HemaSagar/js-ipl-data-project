function teamsWonTossAndMatch(matches) {
    const tossAndMatchesWon = matches.reduce((result, match) => {
        if (match.toss_winner === match.winner) {
            result[match.winner] = (result[match.winner] ?? 0) + 1;
        }

        return result
    }, {});
    return tossAndMatchesWon;

}

module.exports = teamsWonTossAndMatch;

