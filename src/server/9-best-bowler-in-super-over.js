function bestEconomicSuperOverBowler(deliveries) {

    //filtering superOvers and finding balls bowled and runs conceeded for each bowler.
    let superOverDetails = deliveries.filter((delivery) => {
        return Number(delivery.is_super_over) === 1;
    })
    .reduce((result,delivery) => {
        if (result[delivery.bowler]) {
            result[delivery.bowler].runs += Number(delivery.total_runs);
            result[delivery.bowler].balls += 1;

        }
        else {
            result[delivery.bowler] = { runs: Number(delivery.total_runs), balls: 1 };
        }
        return result;
    },{})

    //finding out the economy for each bowler.
    let economyDetails = Object.keys(superOverDetails).reduce((result,bowler) => {
        result[bowler] = {...superOverDetails[bowler]}
        let economy = (superOverDetails[bowler].runs / (superOverDetails[bowler].balls / 6)).toFixed(2);
        result[bowler]['economy'] = Number(economy);

        return result;
    },{})

    // sorting out the bowler with best economy rate.
    let bowler = Object.keys(economyDetails).sort((bowler1, bowler2) => {
        return economyDetails[bowler1].economy - economyDetails[bowler2].economy;
    })
    .slice(0,1);
    return {[bowler]:economyDetails[bowler]};
}



module.exports = bestEconomicSuperOverBowler;