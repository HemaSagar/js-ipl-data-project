const parse = require('csv-parse').parse;
const fs = require('fs');
const path = require('path');


const matchesPerYear = require('./src/server/1-matches-per-year.js')
const matchesWonByTeamPerYear = require('./src/server/2-matches-won-per-team-per-year.js')
const extraRunsCalculate = require('./src/server/3-extra-runs-conceeded-per-team.js')
const economicalBowlers = require('./src/server/4-top-10-economical-bowlers-in-a-year.js')
const teamsWonTossAndMatch = require('./src/server/5-times-team-won-toss-and-match.js')
const highestPlayerOfMatches = require('./src/server/6-highest-player-of-match-awardee-for-each-season.js')
const strikeRateForEachSeason = require('./src/server/7-strike-rate-of-batsman-each-season.js')
const playerDismissedByPlayersHighest = require('./src/server/8-player-dismissed-by-player-highest.js')
const bestEconomicSuperOverBowler = require('./src/server/9-best-bowler-in-super-over.js')


const matchesPath = path.join(__dirname, '/src/data/matches.csv');
const deliveriesPath = path.join(__dirname, '/src/data/deliveries.csv');

const matches = [];
const deliveries = [];

fs.createReadStream(matchesPath)
    .pipe(parse({
        delimiter: ',',
        columns: true,
    }))
    .on("data", (match) => {
        matches.push(match);
    })
    .on("end", () => {
        fs.createReadStream(deliveriesPath)
            .pipe(parse({
                delimiter: ',',
                columns: true,
            }))
            .on("data", (delivery) => {
                deliveries.push(delivery)
            })
            .on("end", () => {

                //1-matches won per year
                const matchesPlayedPerYear = matchesPerYear(matches);

                //writing to the contents of matchesPerYear to matchesPerYear.json file
                const jsonFilePath1 = path.join(__dirname, '/src/public/output/1-matches-per-year.json');
                fs.writeFile(jsonFilePath1, JSON.stringify(matchesPlayedPerYear, null, 2), (err) => {
                    if (err) {
                        console.log(err);
                    }
                })

                //2-matches won per team per year
                const wonPerTeamPerYear = matchesWonByTeamPerYear(matches);

                const jsonFilePath2 =path.join(__dirname, '/src/public/output/2-matches-won-per-team-per-year.json');
                fs.writeFile(jsonFilePath2, JSON.stringify(wonPerTeamPerYear, null, 2), (err) => {
                    if (err) {
                        console.log(err);
                    }
                });

                //3-extra runs conceeded per team for a specific year
                const extraRuns = extraRunsCalculate(matches, deliveries, 2016);

                let jsonFilePath3 = path.join(__dirname, '/src/public/output/3-extra-runs-conceeded-per-team.json');

                fs.writeFile(jsonFilePath3, JSON.stringify(extraRuns, null, 2), (err) => {
                    if (err) {
                        console.log(err);
                    }
                });

                //4 top 10 economical bowlers in a give year
                const economicalTopTen = economicalBowlers(matches, deliveries, 2015)
                let jsonFilePath4 = path.join(__dirname, '/src/public/output/4-top-ten-economical-bowlers-in-a-year.json')
                fs.writeFile(jsonFilePath4, JSON.stringify(economicalTopTen, null, 2), (err) => {
                    if (err) {
                        console.log(err);
                    }
                })

                //5 number of times teams won both matches and toss.
                const tossAndMatchesWon = teamsWonTossAndMatch(matches)

                const jsonFilePath5 = path.join(__dirname, '/src/public/output/5-times-teams-won-toss-and-match.json');

                fs.writeFile(jsonFilePath5, JSON.stringify(tossAndMatchesWon, null, 2), (err) => {
                    if (err) {
                        console.log(err);
                    }
                })

                //6 highest player of matches for each season.

                const eachYearMatchPlayer = highestPlayerOfMatches(matches);

                let jsonFilePath6 = path.join(__dirname, '/src/public/output/6-highest-player-of-match-awardee-for-each-season.json');
                fs.writeFile(jsonFilePath6, JSON.stringify(eachYearMatchPlayer, null, 2), (err) => {
                    if (err) {
                        console.log(err);
                    }
                })

                //7 strikerates of batsmen for each season
                const strikeRate = strikeRateForEachSeason(matches,deliveries);

                let jsonFilePath7 = path.join(__dirname ,"/src/public/output/7-strike-rate-of-batsman-each-season.json");
                fs.writeFile(jsonFilePath7, JSON.stringify(strikeRate,null,2), (err) =>{
                    if(err){
                        console.log(err);
                    }
                })

                //8 max times a bowler dismissed a batsmen.
                let maxDismissed = playerDismissedByPlayersHighest(deliveries)
                let jsonFilePath8 = path.join(__dirname, "/src/public/output/8-player-dismissed-by-player-highest.json");
                fs.writeFile(jsonFilePath8, JSON.stringify(maxDismissed, null, 2), (err) => {
                    if (err) {
                        console.log(err);
                    }
                })




                //9 best economy in super overs
                const bestEconomy = bestEconomicSuperOverBowler(deliveries);

                let jsonFilePath9 = path.join(__dirname, "/src/public/output/9-best-bowler-in-super-over.json");
                fs.writeFile(jsonFilePath9, JSON.stringify(bestEconomy, null, 2), (err) => {
                    if (err) {
                        console.log(err);
                    }
                })

            })
    });
